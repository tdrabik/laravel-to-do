@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-8 offset-2">
            <h4>Edit ToDo</h4>
            <form action="{{ route('task.update',$task) }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $task->description }}</textarea>
                </div>
                <div class="form-group">
                    <a href="{{ route('task.index') }}" class="btn btn-warning">Cancel</a>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
