@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-12">
            <h3>Tasks</h3>
            <p><a href="{{ route('task.create') }}" class="btn btn-sm btn-primary">Create new</a></p>
            <hr>
            <div class="row">
                <div class="col-8"><h4>Task</h4></div>
                <div class="col-4"><h4>Actions</h4></div>
            </div>
            @forelse ($tasks as $item)

                @include('tasks.list')
            @empty
                <div class="alert alert-info">No Any Tasks</div>
            @endforelse
            {{ $tasks->links() }}
        </div>
    </div>
@endsection
