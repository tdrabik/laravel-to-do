<div class="row mb-4">
    <div class="col-8">

        @if($item->is_done)
            <s>{{ $item->description }}</s>
        @else
            {{ $item->description }}
        @endif

    </div>
    <div class="col-4">
        <div class="btn-group">
            <a href="{{ route('task.edit',$item) }}" class="btn btn-sm btn-info mr-2">Edit</a>
            @if($item->is_done)
            <button class="btn btn-success btn-sm" disabled>Done</button>
            @else
                <a href="{{ route('task.done',$item) }}" class="btn btn-warning btn-sm">Done</a>
            @endif
            <form action="{{ route('task.destroy',$item) }}" class="ml-2" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" name="delete" class="btn btn-sm btn-danger">Delete</button>
            </form>
        </div>
    </div>
</div>
