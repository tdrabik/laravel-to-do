<div class="col-md-12">
    @if (session('success'))
        <div class="alert alert-success">
            <h5>Success !</h5>
            <p class="alert-message">{{ session('success') }}</p>
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning">
            <h5>Alert !</h5>
            <p class="alert-message">{{ session('warning') }}</p>
        </div>
    @endif
        @if(count($errors)>0)
            <div class="alert alert-danger" role="alert">
                <h5>Attention</h5>
                @if(session('errors'))
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $error)
                            <li class="alert-message">{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
</div>
