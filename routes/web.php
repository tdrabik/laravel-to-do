<?php

use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth:web'])->group(function(){
    Route::resource('task',TaskController::class);
    Route::get('task/{task}/done',[TaskController::class,'done'])->name('task.done');
});
