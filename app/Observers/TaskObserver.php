<?php

namespace App\Observers;

use App\Models\Task;

class TaskObserver
{
    /**
     * Handle the task "creating" event.
     *
     * @param  Task  $task
     */
    public function creating(Task $task)
    {
        $task->user_id = auth()->id();
    }

    /**
     * Handle the task "updating" event.
     *
     * @param  Task  $task
     */
    public function updating(Task $task)
    {
        //
    }

    /**
     * Handle the task "created" event.
     *
     * @param  Task  $task
     */
    public function created(Task $task)
    {
        //
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  Task  $task
     */
    public function updated(Task $task)
    {
        //
    }

    /**
     * Handle the task "deleted" event.
     *
     * @param  Task  $task
     */
    public function deleted(Task $task)
    {
        //
    }

    /**
     * Handle the task "restored" event.
     *
     * @param  Task  $task
     */
    public function restored(Task $task)
    {
        //
    }

    /**
     * Handle the task "force deleted" event.
     *
     * @param  Task  $task
     */
    public function forceDeleted(Task $task)
    {
        //
    }
}
