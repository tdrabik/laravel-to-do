<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\CreateFormRequest;
use App\Http\Requests\Task\UpdateFormRequest;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = auth()->user()->tasks()->paginate(5);
        return view('tasks.index')
            ->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Task::class);
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFormRequest $request)
    {
        Task::create($request->validated());
        return redirect()->route('task.index')
            ->with('success','Task Has Been Created');;
    }

    /**
     * Display the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        $this->authorize('view', $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $this->authorize('update', $task);
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFormRequest $request, Task $task)
    {
        $this->authorize('update', $task);
        $task->update($request->validated());
        return redirect()->route('task.index')
            ->with('success','Task Has Been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $this->authorize('delete', $task);
        $task->delete();
        return redirect()->route('task.index')
            ->with('success','Task Has Been Deleted');;
    }
    /**
     * MArk as done the specified resource in storage.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function done(Task $task)
    {
        $this->authorize('update', $task);
        $task->update(['is_done' => true]);
        return redirect()->back()->with('success','Task Has Been Completed');
    }
}
