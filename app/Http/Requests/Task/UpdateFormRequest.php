<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFormRequest extends FormRequest
{
    public function rules()
    {
        return [
            'description'=>'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
